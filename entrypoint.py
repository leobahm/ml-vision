import os
from flask import make_response

from flask import Flask, flash, request, redirect, url_for
from werkzeug.utils import secure_filename

from utilities import create_payload, image_encode, clasified_img


app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = "./"
    

@app.get("/")
def index():
    return make_response({"data": True})

@app.post("/send")
def send():
    if request.method == 'POST':
        if 'file' not in request.files:
            return make_response({"file": "is_required"})
        file = request.files['file']
        path = os.path.join(app.config['UPLOAD_FOLDER'], file.filename)
        file.save(path)
        try:
            img = image_encode(path)
            payload = create_payload(img)
            data = clasified_img(payload)
            os.remove(path)
            if data:
                return make_response({"data": data}, 200)
            else:
                return make_response({"data": False}, 200)

        except Exception as error:
            return make_response(error)

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=5000)
