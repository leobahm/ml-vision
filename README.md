# ml-vision



## Getting started

Install Python 3.11.4

## Create virtual environment

```
virtualenv ${env_name}
```

## Activate virtual enviroment

```
cd ${env_name}/Scripts
activate
cd ../..
```

## Install dependences
```
pip install -r requirements.txt
```

## Run project
```
python entrypoint.py
```