from openai import OpenAI
import os

client = OpenAI(api_key="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")

import base64
import requests

# Function to encode the image
def encode_image(image_path):
	with open(image_path, "rb") as image_file:
		return base64.b64encode(image_file.read()).decode('utf-8')

def get_file(name):
	if os.path.exists(name):
		return [f"{name}\{file}" for file in os.listdir(name)]

def image_encode(path):
	try:
		return encode_image(path)
	except:
		raise Exception("error decode image")

def create_payload(en_img):
	"""
	1. Pout Clout Lip Plumping Pen
	2. Halo Glow Liquid Filter
	3. Lash XTNDR
	4. Glow Reviver Lip Oil
	5. O FACE Satin Lipstick
	6. Power Grip Primer
	7. Liquid Poreless Putty Primer
	8. Halo Glow Beauty Wand
	"""
	return {
		"model": "gpt-4-vision-preview",
		"messages": [
			{
			"role": "user",
			"content": [
				{
				"type": "text",
				"text": """From the following list of e.l.f. brand products, which product is shown in the image?
				1. Satin Sheets Lipstick
				2. Halo glow Liquid Filter
				3. Power Grip Primer
				4. Holy! Hydration Face Cream
				5. ⁠Makeup & Mist set
				6. Dewy Coconut ⁠Setting Mist
				7. Stay All Night Blue Light Micro-⁠Setting Mist
				8. Stay All Night Micro-Fine ⁠Setting Mist
				"""
				},
				{
				"type": "image_url",
				"image_url": {
					"url": f"data:image/jpeg;base64,{en_img}"
				}
				}
			]
			}
		],
		"max_tokens": 300
		}

def clasified_img(payload):
	headers = {
		"Content-Type": "application/json",
		"Authorization": f"Bearer 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"
  	}
	response = requests.post("https://api.openai.com/v1/chat/completions", headers=headers, json=payload)
  	# print(response.json())
	try:
		if "Satin Sheets Lipstick".lower() in response.json()["choices"][0]["message"]["content"].lower():
			return "Satin Sheets Lipstick"
		elif "Halo glow Liquid Filter".lower() in response.json()["choices"][0]["message"]["content"].lower():
			return "Halo glow Liquid Filter"
		elif "Power Grip Primer".lower() in response.json()["choices"][0]["message"]["content"].lower():
			return "Power Grip Primer"
		elif "Holy! Hydration Face Cream".lower() in response.json()["choices"][0]["message"]["content"].lower():
			return "Holy! Hydration Face Cream"
		elif "Makeup Mist & Set".lower() in response.json()["choices"][0]["message"]["content"].lower():
			return "Makeup Mist & Set"
		# elif "Setting Mist".lower() in response.json()["choices"][0]["message"]["content"].lower():
		#   return "Setting Mist"
		elif "Dewy Coconut Setting Mist".lower() in response.json()["choices"][0]["message"]["content"].lower():
			return "Dewy Coconut ⁠Setting Mist"
		elif "Stay All Night Blue Light Micro-Setting Mist".lower() in response.json()["choices"][0]["message"]["content"].lower():
			return "Stay All Night Blue Light Micro-Setting Mist"
		elif "Stay All Night Micro-Fine Setting Mist".lower() in response.json()["choices"][0]["message"]["content"].lower():
			return "Stay All Night Micro-Fine Setting Mist"
		else: 
	# if "⁠Makeup Mist & Set".lower() in response.json()["choices"][0]["message"]["content"].lower():
	#   return "⁠Makeup Mist & set"Stay All Night Micro-Fine Setting Mist
	
			print(response.json())
			return False
	except:
		print(response.json())
		raise Exception("error in request")
